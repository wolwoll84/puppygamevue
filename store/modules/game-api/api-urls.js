const BASE_URL = '/v1'

export default {
    DO_FIRST_DATA: `${BASE_URL}/game-data/init`, //get
    DO_PLUS_INGREDIENT: `${BASE_URL}/ingredient/plus`, //put
    DO_CHOOSE_CRUMBS: `${BASE_URL}/choose-puppy/crumbs`, //post
    DO_CHOOSE_BIG: `${BASE_URL}/choose-puppy/big`, //post
    DO_GAME_RESET: `${BASE_URL}/game-data/reset`, //post
}
