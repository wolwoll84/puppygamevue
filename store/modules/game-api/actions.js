import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'

export default {
    [Constants.DO_FIRST_DATA]: (store) => {
        return axios.get(apiUrls.DO_FIRST_DATA)
    },
    [Constants.DO_PLUS_INGREDIENT]: (store) => {
        return axios.put(apiUrls.DO_PLUS_INGREDIENT)
    },
    [Constants.DO_CHOOSE_CRUMBS]: (store) => {
        return axios.post(apiUrls.DO_CHOOSE_CRUMBS)
    },
    [Constants.DO_CHOOSE_BIG]: (store) => {
        return axios.post(apiUrls.DO_CHOOSE_BIG)
    },
    [Constants.DO_GAME_RESET]: (store) => {
        return axios.delete(apiUrls.DO_GAME_RESET)
    },
}
