export default {
    DO_FIRST_DATA: 'game-api/doFirstData',
    DO_PLUS_INGREDIENT: 'game-api/doPlusIngredient',
    DO_CHOOSE_CRUMBS: 'game-api/doChooseCrumbs',
    DO_CHOOSE_BIG: 'game-api/doChooseBig',
    DO_GAME_RESET: 'game-api/doGameReset',
}
